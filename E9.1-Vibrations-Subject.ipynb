{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "# MEC552B — Numerical methods in (solid) mechanics — Martin Genet\n",
    "\n",
    "# L9 — Partial differential equations (Elastodynamics initial/boundary value problem) — The finite element method + Integration schemes \n",
    "\n",
    "# E9.1 — Vibrations — Matthias Rambausek & Martin Genet"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "As usual, let us start with *some* imports…"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# For better printing within jupyter cells (otherwise only the last variable is printed)\n",
    "import IPython\n",
    "IPython.core.interactiveshell.InteractiveShell.ast_node_interactivity = \"all\"\n",
    "\n",
    "# Standard python libraries\n",
    "import math\n",
    "import os\n",
    "import time\n",
    "\n",
    "# Computing libraries\n",
    "import numpy\n",
    "\n",
    "# Meshing libraries\n",
    "import gmsh\n",
    "import meshio\n",
    "\n",
    "# FEniCS\n",
    "import dolfin # dolfin if the main interface to FEniCS\n",
    "dolfin.parameters[\"form_compiler\"][\"cpp_optimize\"] = True\n",
    "dolfin.parameters[\"form_compiler\"][\"optimize\"] = True\n",
    "\n",
    "# VTK and visualization\n",
    "import itkwidgets\n",
    "import myVTKPythonLibrary as myvtk\n",
    "\n",
    "# MEC552 python library\n",
    "import LIB552 as lib"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "## Problem"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "In this exercise we compute and analyze the vibration frequencies and modes of a thin disc (radius $R$, thickness $Z$), made of an isotropic linear elastic material (mass density $\\rho$, Young modulus $E$, Poisson ratio $\\nu$).\n",
    "In principle one would use a (2D) plate model for such a problem, but here we will model it in 3D.\n",
    "\n",
    "Note that this notebook is inspired by an example from [Jeremy Bleyer's COMET website](https://comet-fenics.readthedocs.io/en/latest)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "### Parameters"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "We set some numerical values for the physical parameters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Geometry\n",
    "R = 1.\n",
    "Z = 0.1\n",
    "\n",
    "# Material\n",
    "rho   = 1.\n",
    "E     = 1.\n",
    "nu    = 0.3\n",
    "lmbda = E * nu / (1 + nu) / (1 - 2 * nu)\n",
    "mu    = E / 2 / (1 + nu)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "## Finite element resolution"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q1.\n",
    "Derive the weak formulation of the vibration problem, in terms of the vibration frequency $\\Omega$ and the vibration mode $\\underline{U}$.**\n",
    "\n",
    "Hint:\n",
    " * Simply inject a vibration solution of the form $\\underline{u}\\left(\\underline{x},t\\right) = \\underline{U}\\left(\\underline{x}\\right)\\sin\\left(\\Omega t\\right)$ into the elasto-dynamic equations, and follow the Galerkin procedure to turn the system of local equations into a single global variational equation of the form $\\underline{U},\\Omega ~/~ b_K\\left(\\underline{U}, \\underline{U^*}\\right) = \\Omega^2~b_M\\left(\\underline{U}, \\underline{U^*}\\right) ~ \\forall \\underline{U^*}$, where $b_K$ and $b_M$ are symmetric coercive bilinear forms, both to be expressed in terms of the problem parameters."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q2.\n",
    "Derive the associated discrete generalized eigenproblem, in terms of the vibration frequency $\\Omega$ and the discrete vibration mode $\\underline{\\mathbb{U}}$.**\n",
    "\n",
    "Hint:\n",
    " * Simply inject the discretization $\\underline{U}\\left(\\underline{x}\\right) \\approx {}^{t}{\\underline{\\underline{\\mathbb{N}}}}\\left(\\underline{x}\\right) \\cdot \\underline{\\mathbb{U}}$ into the weak formulation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "### Parameters"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "We define the computation parameters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Number of elements in the radius of the disc\n",
    "N_R = 10\n",
    "\n",
    "# Number of elements in the thickness of the disc\n",
    "N_Z = 10\n",
    "\n",
    "# Target number of computed modes\n",
    "N_modes = 16"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "### Mesh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "We build the mesh, for instance using constructive geometry.\n",
    "In order to have a fine control on the element size (notably, different refinements in the plane in the thickness), we first generate a disc, and then extrude it."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q3.\n",
    "Complete and execute the following code.**\n",
    "\n",
    "Hints:\n",
    "* `factory.addDisk(xc, yc, zc, rx, ry)` creates a (2D) ellipsoid centered in [xc,yc,zc], aligned with the (x,y) plane, with radii [rx,ry], and returns its tag.\n",
    "* `factory.extrude(dimTags=[(2,s1)], dx=dx, dy=dy, dz=dz, numElements=[n])[1][1]` extrudes the (2D) surface tagged `s1` by the vector [dx, dy, dz], creating n layers of elements, and returns the tag of the generated (3D) volume."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Initialization\n",
    "gmsh.initialize()\n",
    "gmsh.clear()\n",
    "\n",
    "## Geometry\n",
    "factory = gmsh.model.occ\n",
    "disk_surf_tag = ### YOUR CODE HERE ###\n",
    "disk_volu_tag = ### YOUR CODE HERE ###\n",
    "\n",
    "# Synchronization, cf., e.g., https://gitlab.onelab.info/gmsh/gmsh/-/blob/master/tutorial/python/t16.py\n",
    "factory.synchronize()\n",
    "\n",
    "# In order to only save nodes and elements of the final mesh\n",
    "# (i.e., not the construction nodes and elements—remember that \n",
    "# unstructured meshers will first mesh the curves, then the\n",
    "# surfaces and the volumes, cf. L5.2), we declare it as a\n",
    "# \"physical\" entity.\n",
    "disk_phys_tag = gmsh.model.addPhysicalGroup(dim=3, tags=[disk_volu_tag])\n",
    "\n",
    "## Mesh\n",
    "mesh_gmsh = gmsh.model.mesh\n",
    "\n",
    "# Characteristic size, cf., e.g., https://gitlab.onelab.info/gmsh/gmsh/-/blob/master/tutorial/python/t16.py\n",
    "mesh_gmsh.setSize(gmsh.model.getEntities(0), R/N_R)\n",
    "\n",
    "# Mesh generation\n",
    "mesh_gmsh.generate(dim=3)\n",
    "\n",
    "# In order to visualize the mesh and perform finite element computation using\n",
    "# FEniCS, we need to convert the mesh from the GMSH format to the VTK & FEniCS\n",
    "# formats. Since there is no direct converter between these formats, we do\n",
    "# that here by writing the mesh to the disc in VTK format using GMSH, which\n",
    "# we can then read in various formats later on.\n",
    "gmsh.write(\"E9.1-Vibrations-mesh.vtk\")\n",
    "\n",
    "# Finalization\n",
    "gmsh.finalize()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "Let us visualize the mesh, using [itkwidgets](https://github.com/InsightSoftwareConsortium/itkwidgets)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mesh_vtk = myvtk.readUGrid(\"E9.1-Vibrations-mesh.vtk\")\n",
    "itkwidgets.view(geometries=mesh_vtk)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "Let us now convert the mesh into the FEniCS format, using [meshio](https://github.com/nschloe/meshio)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mesh_meshio = meshio.read(\"E9.1-Vibrations-mesh.vtk\")\n",
    "meshio.write(\"E9.1-Vibrations-mesh.xdmf\", mesh_meshio)\n",
    "\n",
    "mesh = dolfin.Mesh()\n",
    "dolfin.XDMFFile(\"E9.1-Vibrations-mesh.xdmf\").read(mesh)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "### Finite element and function space"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "We now define the interpolation space, starting with a finite element structure, very similar (though much more complete) to the one of LIB552 used in previous notebooks."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fe = dolfin.VectorElement(\n",
    "    family=\"Lagrange\",\n",
    "    cell=mesh.ufl_cell(), # triangle in 2D, tetrahedron in 3D\n",
    "    degree=1)\n",
    "fe"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "The function space structure is based on the mesh and the finite element, it contains notably the dof manager."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fs = dolfin.FunctionSpace(mesh, fe)\n",
    "fs\n",
    "\n",
    "# Total number of degrees of freedom\n",
    "fs.dim()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "### Finite element matrices"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "Let us start by assembling the mass matrix.\n",
    "Do do so, we define the bilinear form associated to the matrix.\n",
    "It must be bilinear in the solution function (called trial function) and the test function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# We define the trial and test functions.\n",
    "# These are the objects that formally get\n",
    "# replaced by the linear combinations of\n",
    "# shape functions in order to define the\n",
    "# associated matrix.\n",
    "U_tria = dolfin.TrialFunction(fs)\n",
    "U_test = dolfin.TestFunction(fs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q4.\n",
    "Complete and execute the following code**\n",
    "\n",
    "Hints:\n",
    " * `dolfin.inner(f, g)` returns the inner (i.e., scalar) product between the two tensors `f` & `g`.\n",
    "(If `f` & `g` are zero-order tensors, i.e., scalars, it is a simple product;\n",
    "if they are first-order tensors, i.e., vectors, it is a dot product;\n",
    "if they are second-order tensors, i.e., matrices, it is a double dot product; …)\n",
    " * `f * dolfin.dx(domain=mesh)` is the variational form corresponding to the integral of the scalar function `f` over the domain `mesh`.\n",
    " * `dolfin.assemble(form)` will assemble the variational form `form`.\n",
    "(If `form` has arity 0, i.e., if it is a scalar, independent from any test or trial function, it will return its integral, i.e., a scalar;\n",
    "if it has arity 1, i.e., if it is a linear form of a test function, but independent from any trial function, it will return its integral as a finite element vector;\n",
    "if it has arity 2, i.e., if it is a bilinear form of a test function and a trial function, it will return its integral as a finite element matrix.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# We turn rho into a dolfin.Constant,\n",
    "# so that if we change its numerical value,\n",
    "# FEniCS will not regenerate and recompile\n",
    "# the variational forms and associated\n",
    "# assembly procedures.\n",
    "rho = dolfin.Constant(rho)\n",
    "\n",
    "# Mass variational form\n",
    "M_form = ### YOUR CODE HERE ###\n",
    "\n",
    "# Mass matrix\n",
    "M = ### YOUR CODE HERE ###"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "We now assemble the stiffness matrix."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q5.\n",
    "Complete and execute the following code**\n",
    "\n",
    "Hints:\n",
    " * `dolfin.grad(f)` returns the gradient of the tensor `f`.\n",
    "(If `f` is a zero-order tensor, i.e., a scalar, it is a first-order tensor, i.e., a vector;\n",
    "if it is a first-order tensor, i.e., a vector, it is a second-order tensor, i.e., a matrix; …)\n",
    " * `dolfin.sym(f)` returns the symmetric part of the second order tensor `f`, i.e, the second order tensor (f + f.T)/2.\n",
    " * `dolfin.tr(f)` returns the trace of the second order tensor `f`.\n",
    " * `dolfin.Identity(d)` returns the identity second order tensor in dimension `d`.\n",
    " * `dolfin.inner(f, g)` returns the inner (i.e., scalar) product between the two tensors `f` & `g`.\n",
    "(If `f` & `g` are zero-order tensors, i.e., scalars, it is a simple product;\n",
    "if they are first-order tensors, i.e., vectors, it is a dot product;\n",
    "if they are second-order tensors, i.e., matrices, it is a double dot product; …)\n",
    " * `f * dolfin.dx(domain=mesh)` is the variational form corresponding to the integral of the scalar function `f` over the domain `mesh`.\n",
    " * `dolfin.assemble(form)` will assemble the variational form `form`.\n",
    "(If `form` has arity 0, i.e., if it is a scalar, independent from any test or trial function, it will return its integral, i.e., a scalar;\n",
    "if it has arity 1, i.e., if it is a linear form of a test function, but independent from any trial function, it will return its integral as a finite element vector;\n",
    "if it has arity 2, i.e., if it is a bilinear form of a test function and a trial function, it will return its integral as a finite element matrix.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# We turn lambda & mu into dolfin.Constants.\n",
    "lmbda = dolfin.Constant(lmbda)\n",
    "mu    = dolfin.Constant(mu)\n",
    "\n",
    "# Stress (associated to the trial function)\n",
    "eps_tria = ### YOUR CODE HERE ###\n",
    "sig_tria = ### YOUR CODE HERE ###\n",
    "\n",
    "# Strain (associated to the test function)\n",
    "eps_test = ### YOUR CODE HERE ###\n",
    "\n",
    "# Stiffness variational form\n",
    "K_form = ### YOUR CODE HERE ###\n",
    "\n",
    "# Stiffness matrix\n",
    "K = ### YOUR CODE HERE ###"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "### Eigenvalue solver"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "The generalized eigenvalue problem will be solved using [SLEPc](https://slepc.upv.es), which is the *de facto* standard for this type of problems, and is integrated into FEniCS.\n",
    "It can directly operate on the [PETSc](https://www.mcs.anl.gov/petsc) arrays assembled by FEniCS.\n",
    "\n",
    "We create the solver."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# (In order to create the SLEPc solver, we need to access the PETSc objects underlying the dolfin arrays…)\n",
    "K_petsc = dolfin.as_backend_type(K)\n",
    "M_petsc = dolfin.as_backend_type(M)\n",
    "eigensolver = dolfin.SLEPcEigenSolver(K_petsc, M_petsc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "The SLEPc eigenvalue solver can handle a variety of different problems and can (needs to) be tuned properly.\n",
    "For the full set of settings one can refer to [the SLEPc manual](https://slepc.upv.es/documentation/manual.htm).\n",
    "Searching for all eigenvalues is practically prohibitive, so we only compute a subset.\n",
    "In the present case, we are interested in the smallest (magnitude) eigenvalues, which correspond to low vibration frequencies.\n",
    "However, eigenvalue solvers are designed to compute the largest eigenvalues, so that we have to tell the solver to perform an 'inversion' of the problem, which is done through the option 'shift-and-invert'—the shift in our case is $0$, which is the smallest (in magnitude) eigenvalue."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Problem type: \"gen_hermitian\" -> generalized hermitian, i.e.,\n",
    "# generalized eigenvalues with a self-adjoint (i.e., in case of\n",
    "# real numbers, symmetric) linear operators (matrices).\n",
    "eigensolver.parameters[\"problem_type\"] = \"gen_hermitian\"\n",
    "\n",
    "# General transform\n",
    "eigensolver.parameters[\"spectral_transform\"] = \"shift-and-invert\"\n",
    "\n",
    "# Actual shift value: here zero\n",
    "eigensolver.parameters[\"spectral_shift\"] = 0."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "Now let us solve for *some* eigenvalues and eigenvectors.\n",
    "*Some* because, as mentioned above, solving for eigenvalues is expensive.\n",
    "In particular it becomes more expensive the further one departs from the target values, which is $0$ in our case.\n",
    "Funny thing is, you never know how much modes you will have at the end!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Solve the eigenproblem, asking for a certain number of modes…\n",
    "eigensolver.solve(N_modes)\n",
    "print(\"N_modes:\", N_modes)\n",
    "\n",
    "# The number of modes actually computed…\n",
    "N_pairs = eigensolver.get_number_converged()\n",
    "print(\"N_pairs:\", N_pairs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "The solver provides us with real and imaginary parts of eigenvalues and eigenvectors, so we extract the real parts, making sure the imaginary parts are small."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_real(pair):\n",
    "    eval_real, eval_imag, evec_real, evec_imag = pair\n",
    "    if (eval_imag > 1e-12):\n",
    "        raise RuntimeError(\"Imaginary part of eigenvalue is not zero (< 1e-12) but {:.16e}\".format(eval_imag))\n",
    "    return eval_real, evec_real\n",
    "\n",
    "pairs = [get_real(eigensolver.get_eigenpair(k_pair)) for k_pair in range(N_pairs)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "We print the eigenvalues."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print([pair[0] for pair in pairs])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q6.\n",
    "What do you notice regarding the first six eigenvalues?\n",
    "Why is that?**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q7.\n",
    "What do you notice regarding the following eigenvalues?\n",
    "Why is that?**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "We save the eigenvectors for further visualization.\n",
    "Note that SLEPc scales the eigenvectors such that $\\mathbf{v} \\cdot \\mathbf{M} \\cdot \\mathbf{v} = 1$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Cleaning\n",
    "os.system(\"rm -f E9.1-Vibrations/*\")\n",
    "\n",
    "# Output file\n",
    "pvd_file = dolfin.File(\"E9.1-Vibrations/modes.pvd\")\n",
    "\n",
    "# Function object for proper output\n",
    "eigenvector = dolfin.Function(fs, name=\"eigenvector\")\n",
    "\n",
    "# Writing the undeformed mesh\n",
    "pvd_file.write(eigenvector, 0)\n",
    "\n",
    "# Loop over computed pairs\n",
    "for pair in pairs:\n",
    "    # Eigenvalue\n",
    "    eigenvalue = pair[0]\n",
    "    \n",
    "    # Eigenvector\n",
    "    eigenvector.vector().set_local(pair[1].get_local())\n",
    "\n",
    "    # Write the eigenvector, the time stamp corresponding to the eigenvalue\n",
    "    pvd_file.write(eigenvector, eigenvalue)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "### Visualization"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "viewer = lib.DisplacementViewer(pvd_folder=\"E9.1-Vibrations\", pvd_filename=\"modes.pvd\", state_label=\"frequency\")\n",
    "viewer.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "### Analysis"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q8.\n",
    "What happens if you ask for (much) more modes?**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q9.\n",
    "What is the influence of the physical parameters (notably, the thickness, the Young modulus) on the results?**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q10.\n",
    "What is the influence of the numerical parameters (notably, the mesh size) on the results?**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "## Bonus"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q11.\n",
    "Block the rigid body motion while letting the plate free.\n",
    "How does that impact the results?**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q12.\n",
    "Clamp the plate at its perimeter.\n",
    "How does that impact the results?**"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.10.5 ('fenics-2019-py310')",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.5"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  },
  "vscode": {
   "interpreter": {
    "hash": "465073c950d46f2fb6daa5cc7dc8ea8ee4ad3d47a8c32729612b391401404a90"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
