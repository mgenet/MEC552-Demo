{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "# MEC552B — Numerical methods in (solid) mechanics — Martin Genet\n",
    "\n",
    "# L3 — Differential equations/systems — Integration schemes\n",
    "\n",
    "# E3 — Spring-mass dynamics"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "As usual, we start with some imports…"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# For better printing within jupyter cells\n",
    "import IPython\n",
    "IPython.core.interactiveshell.InteractiveShell.ast_node_interactivity = \"all\"\n",
    "\n",
    "# Basic python libraries\n",
    "import math\n",
    "import matplotlib.pyplot as mpl # doc: https://matplotlib.org/tutorials\n",
    "import numpy                    # doc: https://numpy.org/doc/stable\n",
    "import sympy                    # doc: https://docs.sympy.org"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "## Problem"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "We consider the following simple spring-mass system:\n",
    "<div style=\"text-align:center\">\n",
    "    <img src=\"FIGURES/E3-spring.svg\" width=600/>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "First we write the behavior of the spring:\n",
    "$$\n",
    "    F_s = k \\left(u - u_0\\right),\n",
    "$$\n",
    "as well as the equilibrium of the mass:\n",
    "$$\n",
    "    m \\ddot{u} = - F_s + F_m.\n",
    "$$\n",
    "We then introduce boundary conditions, traducing the infinite stiffness of the frame and the fact that we will study free vibrations of the mass:\n",
    "$$\n",
    "    \\begin{cases}\n",
    "        u_0 = 0 \\\\\n",
    "        F_m = 0\n",
    "    \\end{cases},\n",
    "$$\n",
    "as well as initial conditions:\n",
    "$$\n",
    "    \\begin{cases}\n",
    "        u\\left(t=0\\right) = u_i \\\\\n",
    "        \\dot{u}\\left(t=0\\right) = v_i\n",
    "    \\end{cases},\n",
    "$$\n",
    "where $u_i$ & $v_i$ are parameters of the problem.\n",
    "Combining these equations, we end up with the following differential problem:\n",
    "$$\n",
    "    \\begin{cases}\n",
    "    \\ddot{u} + \\omega^2 u = 0\\\\\n",
    "    u\\left(t=0\\right) = u_i \\\\\n",
    "    \\dot{u}\\left(t=0\\right) = v_i\n",
    "    \\end{cases},\n",
    "$$\n",
    "where $\\omega = \\sqrt{\\frac{k}{m}}$ is the pulsation of the system.\n",
    "\n",
    "Let us recall that the total energy of the system is the sum of the kinetic and elastic energies:\n",
    "$$\n",
    "    E\n",
    "    = \\mathcal{K} + \\mathcal{E}\n",
    "    = \\frac{1}{2} m \\dot{u}^2 + \\frac{1}{2} k u^2,\n",
    "$$\n",
    "and is conserved over time."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "## Exact solution"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q1.\n",
    "What is the exact solution of this problem?**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "We will now derive this solution analytically.\n",
    "Let us first define some symbolic expressions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t     = sympy.Symbol(    \"t\", real=True)\n",
    "m     = sympy.Symbol(    \"m\", real=True)\n",
    "k     = sympy.Symbol(    \"k\", real=True)\n",
    "omega = sympy.Symbol(\"omega\", real=True)\n",
    "ui    = sympy.Symbol(  \"u_i\", real=True)\n",
    "vi    = sympy.Symbol(  \"v_i\", real=True)\n",
    "u     = sympy.Function(\"u\"  , real=True)(t)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "We can now define and solve the differential problem."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q2.\n",
    "Complete and execute the following code.**\n",
    "\n",
    "Hints:\n",
    "* `sympy.dsolve(eq, func, ics)` solves the differential equation `eq = 0` (no need to specify `= 0` in `sympy.dsolve`) for the function `func` with initial conditions `ics` (initial conditions such as $f(x=0) = f'(x=0) = 0$ would be expressed as `{f.subs(x,0):0, f.diff(x).subs(x,0):0}`).\n",
    "(Note that it returns an equation `func = …`, hence the need for `rhs` —right hand side— to extract the solution itself.)\n",
    "* In `sympy` the derivative of an expression `f` with respect to a symbol `x` is `f.diff(x)`.\n",
    "The second derivative is `f.diff(x).diff(x)` or `f.diff(x, x)`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Here we solve the differential problem, store the solution into u_sol\n",
    "u_sol = sympy.dsolve(\n",
    "    eq=### YOUR CODE HERE ###,\n",
    "    func=u,\n",
    "    ics={### YOUR CODE HERE ###}).rhs\n",
    "print(\"u_sol:\"); u_sol"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "We can now compute some energetic quantities."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Now we compute the kinetic energy, first its general expression in terms of u\n",
    "E_k = m * u.diff(t)**2 / 2\n",
    "print(\"E_k:\"); E_k\n",
    "# And then the kinetic energy of the solution\n",
    "# (doit() is sometimes required to force sympy to fully evaluate expressions)\n",
    "E_k_sol = E_k.subs(u, u_sol).doit()\n",
    "print(\"E_k_sol:\"); E_k_sol\n",
    "\n",
    "# Same thing for the elastic energy: first its general expression in terms of u\n",
    "E_e = k * u**2 / 2\n",
    "print(\"E_e:\"); E_e\n",
    "# And then the elastic energy of the solution\n",
    "E_e_sol = E_e.subs(u, u_sol).doit()\n",
    "print(\"E_e_sol:\"); E_e_sol\n",
    "\n",
    "# Same thing for the total energy: first its general expression in terms of u\n",
    "E = E_k + E_e\n",
    "print(\"E:\"); E\n",
    "# And then the total energy of the solution\n",
    "E_sol = E.subs(u, u_sol).doit()\n",
    "print(\"E_sol:\"); E_sol"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "We can also plot the solution.\n",
    "To do so we need to specify numerical values to all symbolic variables.\n",
    "We will do so in a generic way."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q3.\n",
    "Specify numerical values for the problem parameters.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ui_    = # m    ### YOUR CODE HERE ###\n",
    "vi_    = # m/s  ### YOUR CODE HERE ###\n",
    "k_     = # N/m  ### YOUR CODE HERE ###\n",
    "m_     = # Kg   ### YOUR CODE HERE ###\n",
    "omega_ = # 1/s  ### YOUR CODE HERE ###"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "We will now store them into a python dictionary, with `symbolic:numeric` pairs, such that we can replace all symbolic variables into an expression at once."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subs_dict        = {}\n",
    "subs_dict[ui]    = ui_\n",
    "subs_dict[vi]    = vi_\n",
    "subs_dict[k]     = k_\n",
    "subs_dict[m]     = m_\n",
    "subs_dict[omega] = omega_\n",
    "print(\"subs_dict:\"); subs_dict\n",
    "print(\"u_sol.subs(subs_dict):\"); u_sol.subs(subs_dict) # Note that this is still a function of t!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "We can now plot the solution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# The solution\n",
    "sympy.plot(\n",
    "    u_sol.subs(subs_dict), # The function\n",
    "    (t,0,10),              # The variable and the range\n",
    "    xlabel=\"t (s)\",\n",
    "    ylabel=\"u (m)\");\n",
    "\n",
    "# The energies\n",
    "p = sympy.plot(\n",
    "    E_k_sol.subs(subs_dict),\n",
    "    E_e_sol.subs(subs_dict),\n",
    "    E_sol.subs(subs_dict),\n",
    "    (t,0,10),\n",
    "    xlabel=\"t (s)\",\n",
    "    ylabel=\"Energy (J)\",\n",
    "    show=False);\n",
    "p[0].line_color = \"blue\"   # Only way to specify different colors for different curves in sympy.plot…\n",
    "p[1].line_color = \"red\"    # (If you want you can try using line_colors=[\"blue\", \"red\", \"green\"] within sympy.plot…)\n",
    "p[2].line_color = \"green\"\n",
    "p.show()\n",
    "\n",
    "# The phase plot\n",
    "sympy.plot_parametric(\n",
    "    u_sol.subs(subs_dict),\n",
    "    u_sol.diff(t).subs(subs_dict),\n",
    "    (t,0,10),\n",
    "    xlabel=\"u (m)\",\n",
    "    ylabel=\"v (m/s)\");"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q4.\n",
    "Do the results make sense to you?\n",
    "Feel free to modify the numerical values of the parameters and analyze the impact on the solution.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "## Numerical resolution"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "We will now compute approximated solutions based on some of the integration schemes seen in class."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q5.\n",
    "Transform this second-order differential equation into a first-order system of differential equations.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q6.\n",
    "Write the solution update associated to the forward/explicit Euler, backward/implicit Euler & midpoint schemes.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "We will now implement these schemes.\n",
    "In order to make the code clean and efficient, for each scheme we will have one initialization function (which we call only once, at the beginning of the computation, and where we prepare all necessary variables; note that they have nothing to do with the initial condition, which will be handled later) & one integration function (which we call at each time step, to update the solution), and we will store them within a class."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q7.\n",
    "Complete and execute the following code.**\n",
    "\n",
    "Hints:\n",
    "* `numpy.array([[a, b], [c, d]])` returns the matrix $\\begin{pmatrix}a & b \\\\ c & d\\end{pmatrix}$ as a 2D array.\n",
    "* `numpy.eye(n)` returns the identify matrix of size `n` as a 2D array.\n",
    "* `numpy.dot(A, B)` returns the matrix product (i.e., contraction on the inner indices) between arrays `A` & `B`.\n",
    "* `numpy.linalg.solve(M, V)` returns `X` the solution of the system `M.X = V`, where `M` is a 2D array and `V` is a 1D array."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Parent class for integration schemes.\n",
    "# Here it is empty, but it could contain variables and functions that are useful for all schemes,\n",
    "#  so that we do not have to copy-paste the code inside each child class.\n",
    "class IntegrationScheme():\n",
    "    pass\n",
    "\n",
    "class ForwardIntegrationScheme(IntegrationScheme): # This is how you stipulate that ForwardIntegrationScheme\n",
    "                                                   # derives from (a.k.a., is a child class of) IntegrationScheme\n",
    "    # This function is called when an object of the class is instantiated,\n",
    "    #  for instance with `integration_scheme = ForwardIntegrationScheme(omega, dt)`.\n",
    "    # In this function we prepare everything such that the actual integration function is concise and fast.\n",
    "    # It takes as an argument the various parameters, physical and numerical, of the problem.\n",
    "    # Note that `self` is the mandatory first keyword argument of each class function (a.k.a., method);\n",
    "    #  it represents the object itself (once it is created),\n",
    "    #  but is omitted when actually calling the functions.\n",
    "    # In this example, the variable `W` (not mandatory, just an example) is defined within the `__init__` function,\n",
    "    #  but will not be available in other class functions.\n",
    "    # Conversely, the variable `A` (not mandatory either, just an example as well) is stored within the object itself,\n",
    "    #  so it is available in all class functions by calling `self.A`.\n",
    "    def __init__(self, omega, dt):\n",
    "        W      = ### YOUR CODE HERE ###\n",
    "        self.A = ### YOUR CODE HERE ###\n",
    "\n",
    "    # Integration function\n",
    "    # This function takes as an argument the solution at the current time step,\n",
    "    #  and returns the solution at the next time step.\n",
    "    # It is called with `integration_scheme.integrate(Yt)`\n",
    "    #  (note again that `self` is omitted when calling the function).\n",
    "    def integrate(self, Yt):\n",
    "        return ### YOUR CODE HERE ###\n",
    "\n",
    "class BackwardIntegrationScheme(IntegrationScheme):\n",
    "    def __init__(self, omega, dt):\n",
    "        W      = ### YOUR CODE HERE ###\n",
    "        self.A = ### YOUR CODE HERE ###\n",
    "\n",
    "    def integrate(self, Yt):\n",
    "        return ### YOUR CODE HERE ###\n",
    "\n",
    "class MidPointIntegrationScheme(IntegrationScheme):\n",
    "    def __init__(self, omega, dt):\n",
    "        W      = ### YOUR CODE HERE ###\n",
    "        self.A = ### YOUR CODE HERE ###\n",
    "        self.B = ### YOUR CODE HERE ###\n",
    "\n",
    "    def integrate(self, Yt):\n",
    "        return ### YOUR CODE HERE ###"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q8.\n",
    "For each scheme, what would happen if the differential equation was nonlinear?**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q9.\n",
    "For each scheme, what would happen if the system had many degrees of freedom?**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "We now actually run the computation and plot the solution.\n",
    "Since all our integration scheme classes have the same interface (i.e., inputs & outputs), we can choose which one we want at the beginning, and then perform the integration independently from our choice."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q10.\n",
    "Complete and execute the following code.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "### Parameters\n",
    "\n",
    "# Physical parameters\n",
    "ui_    = # m    ### YOUR CODE HERE ###\n",
    "vi_    = # m/s  ### YOUR CODE HERE ###\n",
    "k_     = # N/m  ### YOUR CODE HERE ###\n",
    "m_     = # Kg   ### YOUR CODE HERE ###\n",
    "omega_ = # 1/s  ### YOUR CODE HERE ###\n",
    "T      = # s    ### YOUR CODE HERE ###\n",
    "\n",
    "# Numerical parameters and options\n",
    "dt = # s  ### YOUR CODE HERE ###\n",
    "\n",
    "# integration_scheme = ForwardIntegrationScheme(omega=omega_, dt=dt)\n",
    "# integration_scheme = BackwardIntegrationScheme(omega=omega_, dt=dt)\n",
    "integration_scheme = MidPointIntegrationScheme(omega=omega_, dt=dt)\n",
    "\n",
    "### Initialization\n",
    "\n",
    "# We are using a fixed time step and duration, so we know the number of time steps\n",
    "n_t = int(T/dt) + 1\n",
    "t_lst = numpy.linspace(0, T, n_t)\n",
    "\n",
    "# Initialization of the solution vector\n",
    "Y_lst = numpy.empty((n_t, 2))\n",
    "\n",
    "### Integration\n",
    "\n",
    "# Initial conditions\n",
    "Y_lst[0,0] = ### YOUR CODE HERE ###\n",
    "Y_lst[0,1] = ### YOUR CODE HERE ###\n",
    "\n",
    "# Loop over time steps\n",
    "for k_t in range(n_t-1):\n",
    "    Y_lst[k_t+1,:] = ### YOUR CODE HERE ###\n",
    "\n",
    "### Post-processing\n",
    "\n",
    "# Split of the solution vector into displacement and velocity\n",
    "u_lst = Y_lst[:,0]\n",
    "v_lst = Y_lst[:,1]\n",
    "\n",
    "# Energies\n",
    "E_k_lst = ### YOUR CODE HERE ###\n",
    "E_e_lst = ### YOUR CODE HERE ###\n",
    "E_lst   = ### YOUR CODE HERE ###"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "subs_dict        = {}\n",
    "subs_dict[ui]    = ui_\n",
    "subs_dict[vi]    = vi_\n",
    "subs_dict[m]     = m_\n",
    "subs_dict[k]     = k_\n",
    "subs_dict[omega] = omega_\n",
    "\n",
    "# Analytical solution (since one cannot mix sympy & matplotlib plots easily, it needs to be \"discretized\")\n",
    "t_lst2 = numpy.linspace(0, T, int(30*T*omega_/2/math.pi)+1)\n",
    "u_sol_   = [        u_sol.subs(subs_dict).subs(t,t_) for t_ in t_lst2]\n",
    "v_sol_   = [u_sol.diff(t).subs(subs_dict).subs(t,t_) for t_ in t_lst2]\n",
    "E_k_sol_ = [      E_k_sol.subs(subs_dict).subs(t,t_) for t_ in t_lst2]\n",
    "E_e_sol_ = [      E_e_sol.subs(subs_dict).subs(t,t_) for t_ in t_lst2]\n",
    "E_sol_   = [        E_sol.subs(subs_dict).subs(t,t_) for t_ in t_lst2]\n",
    "\n",
    "mpl.figure();\n",
    "mpl.plot(t_lst , u_lst, marker='+', linestyle=\"\", label=\"approx\");\n",
    "mpl.plot(t_lst2, u_sol_                         , label=\"exact\" );\n",
    "mpl.axhline(color=\"black\", linewidth=0.5);\n",
    "mpl.xlabel('t (s)');\n",
    "mpl.ylabel('u (m)');\n",
    "mpl.legend();\n",
    "\n",
    "mpl.figure();\n",
    "mpl.plot(t_lst , v_lst, marker='+', linestyle=\"\", label=\"approx\");\n",
    "mpl.plot(t_lst2, v_sol_                         , label=\"exact\" );\n",
    "mpl.axhline(color=\"black\", linewidth=0.5);\n",
    "mpl.xlabel('t (s)');\n",
    "mpl.ylabel('v (m/s)');\n",
    "mpl.legend();\n",
    "\n",
    "mpl.figure();\n",
    "mpl.plot(u_lst , v_lst, marker='+', linestyle=\"\", label=\"approx\");\n",
    "mpl.plot(u_sol_, v_sol_                         , label=\"exact\" );\n",
    "mpl.axhline(color=\"black\", linewidth=0.5);\n",
    "mpl.axvline(color=\"black\", linewidth=0.5);\n",
    "mpl.xlabel('u (m/s)');\n",
    "mpl.ylabel('v (m/s)');\n",
    "mpl.legend();\n",
    "\n",
    "mpl.figure();\n",
    "mpl.plot(t_lst , E_k_lst, marker='+', linestyle=\"\", color=\"blue\" , label=\"approx\");\n",
    "mpl.plot(t_lst2, E_k_sol_                         , color=\"blue\" , label=\"exact\" );\n",
    "mpl.plot(t_lst , E_e_lst, marker='+', linestyle=\"\", color=\"red\"  , label=\"approx\");\n",
    "mpl.plot(t_lst2, E_e_sol_                         , color=\"red\"  , label=\"exact\" );\n",
    "mpl.plot(t_lst , E_lst  , marker='+', linestyle=\"\", color=\"green\", label=\"approx\");\n",
    "mpl.plot(t_lst2, E_sol_                           , color=\"green\", label=\"exact\" );\n",
    "mpl.axhline(color=\"black\", linewidth=0.5);\n",
    "mpl.xlabel('t (s)');\n",
    "mpl.ylabel('E (J)');\n",
    "mpl.legend();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q11.\n",
    "For each scheme, what is the impact of the time step on the solution?**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q12.\n",
    "What can you say about the energy preservation properties of each scheme?**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "## Bonus"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q13.\n",
    "Add a damper to the system.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q14.\n",
    "Instead of a single spring & mass, consider a system made of many springs & masses (& dampers) in series.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q15. Study the (very) long term response of each scheme. What do you notice?**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q16. Study (theoretically and/or numerically) the pulsation of the discrete system. What do you notice?**"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.8.13 ('MEC552')",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.13"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": false,
   "autoclose": false,
   "autocomplete": false,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": true,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "202.8px"
   },
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "oldHeight": 673.016666,
   "position": {
    "height": "40px",
    "left": "1390px",
    "right": "20px",
    "top": "120px",
    "width": "350px"
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "varInspector_section_display": "none",
   "window_display": false
  },
  "vscode": {
   "interpreter": {
    "hash": "96aa456a741804e957593b3d1f005f59862697d834b323ad509ac5340fbdf582"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
